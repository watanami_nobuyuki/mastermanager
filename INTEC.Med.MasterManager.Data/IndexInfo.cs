﻿namespace INTEC.Med.MasterManager
{
    public class IndexInfo
    {
        public string IndexTitle { get; set; }
        public string IndexPath { get; set; }
    }
}