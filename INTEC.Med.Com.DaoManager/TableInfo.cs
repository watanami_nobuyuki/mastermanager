﻿namespace INTEC.Med.Com.DaoManager
{
    public class TableInfo
    {
        public string MdbFullName { get; set; }
        public string TableName { get; set; }
        public string Description { get; set; }
    }
}