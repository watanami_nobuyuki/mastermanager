﻿using System.Collections.Generic;

namespace INTEC.Med.Com.DaoManager
{
    public class DaoManager
    {
        private DAO.DBEngine _dBEngine;
        private DAO.Workspace _workspace;
        private DAO.Database _database;

        /// <summary>
        /// コンストラクタ（DB接続開始）
        /// </summary>
        /// <param name="mdbFullPath">接続MDB名</param>
        public DaoManager(string mdbFullPath)
        {
            //インスタンス作成
            this._dBEngine = new DAO.DBEngineClass();
            this._workspace = _dBEngine.Workspaces[0];
            this._database = _workspace.OpenDatabase(mdbFullPath);
        }

        /// <summary>
        /// テーブル一覧の取得
        /// </summary>
        public List<TableInfo> StoreTableList()
        {
            List<TableInfo> storedTableInfo = new List<TableInfo>();
            foreach (DAO.TableDef tdb in _database.TableDefs)
            {
                //テーブル名、フルパス、テーブル説明をListに格納
                if ((!tdb.Name.Contains("MSys")) && (tdb.Connect == ""))
                {
                    TableInfo tableInfo = new TableInfo();
                    tableInfo.TableName = tdb.Name;
                    tableInfo.MdbFullName = _database.Name;
                    try
                    {
                        tableInfo.Description = tdb.Properties["DESCRIPTION"].Value.ToString();
                    }
                    catch (System.Runtime.InteropServices.COMException) { }
                    storedTableInfo.Add(tableInfo);
                }
            }
            return storedTableInfo;
        }
    }
}