﻿using INTEC.Med.CommonLibrary;                           //add
using INTEC.Med.MasterManager.ViewsAbstraction;     //add
using log4net;                                           //add
using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace INTEC.Med.MasterManager
{
    public partial class IndexSelectorForm : MaterialSkin.Controls.MaterialForm, IIndexSelectorView
    {
        #region _member変数

        private ILog _log = LogFetcher.GetLogger();
        private string _defaultOpenPath;

        public event GetLastOpenPathHandler _getLastOpenPath;

        public event SetLastOpenPathHandler _setLastOpenPath;

        public event IsContainsMDBHandler _isContaninsMDB;

        public event GetMDBFileListHandler _getMDBFileList;

        public event AddIndexHandler _addIndex;

        #endregion _member変数

        #region プロパティ（インタフェース用）

        public string FormTitle {
            get { return this.Text; }
            set { this.Text = value; }
        }

        public string DefaultOpenPath {
            get { return _defaultOpenPath; }
            set { _defaultOpenPath = value; }
        }

        public string SelectedPath {
            get { return uxSelectedPathText.Text; }
            set { uxSelectedPathText.Text = value; }
        }

        #endregion プロパティ（インタフェース用）

        public IndexSelectorForm()
        {
            _log.Info(this.Text + " Initialize");
            InitializeComponent();
            SelectedPath = _defaultOpenPath;
        }

        #region イベントPRESENTER⇒VIEW

        public void ShowView()
        {
            _log.Info(this.Text + " 表示");
            this.ShowDialog();

            SelectedPath = _defaultOpenPath;
        }

        public void CloseView()
        {
            this.Close();
        }

        public void HandleError(Exception ex)
        {
            _log.Info("エラーメッセージ表示");
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        #endregion イベントPRESENTER⇒VIEW

        private void UxSelectMDBFolderCommand_Click(object sender, EventArgs e)
        {
            using (CommonOpenFileDialog dialog = new CommonOpenFileDialog())
            {
                dialog.InitialDirectory = SelectedPath;
                dialog.IsFolderPicker = true;
                if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
                {
                    if (_isContaninsMDB(dialog.FileName))
                    {
                        List<string> tmp = _getMDBFileList(dialog.FileName);
                        uxSelectedPathItemsDLV.BeginUpdate();
                        uxSelectedPathItemsDLV.DataSource = tmp.Select(x => new { FileName = x }).ToList();
                        foreach (ColumnHeader ch in uxSelectedPathItemsDLV.Columns)
                        {
                            ch.Width = -1;
                        }
                        uxSelectedPathItemsDLV.EndUpdate();
                    }
                    SelectedPath = dialog.FileName;
                    _setLastOpenPath(dialog.FileName);
                }
            }
        }

        private void IndexSelectorForm_Load(object sender, EventArgs e)
        {
            SelectedPath = string.IsNullOrEmpty(_getLastOpenPath()) ? _defaultOpenPath : _getLastOpenPath();
        }

        private void uxCloseCommand_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void uxAddIndexCommand_Click(object sender, EventArgs e)
        {
            _addIndex(SelectedPath,uxIndexTitleText.Text);
        }
    }
}