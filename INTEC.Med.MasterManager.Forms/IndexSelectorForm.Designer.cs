﻿using System;

namespace INTEC.Med.MasterManager
{
    partial class IndexSelectorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.uxSelectPathCommand = new MaterialSkin.Controls.MaterialFlatButton();
            this.uxSelectedPathText = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.uxIndexTitleText = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.uxCloseCommand = new MaterialSkin.Controls.MaterialFlatButton();
            this.uxAddIndexCommand = new MaterialSkin.Controls.MaterialFlatButton();
            this.uxSelectedPathItemsDLV = new BrightIdeasSoftware.DataListView();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uxSelectedPathItemsDLV)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 65);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.uxIndexTitleText);
            this.splitContainer1.Panel2.Controls.Add(this.uxCloseCommand);
            this.splitContainer1.Panel2.Controls.Add(this.uxAddIndexCommand);
            this.splitContainer1.Panel2.Controls.Add(this.uxSelectedPathItemsDLV);
            this.splitContainer1.Size = new System.Drawing.Size(552, 731);
            this.splitContainer1.SplitterDistance = 32;
            this.splitContainer1.TabIndex = 27;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.uxSelectPathCommand);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.uxSelectedPathText);
            this.splitContainer2.Panel2.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.splitContainer2.Size = new System.Drawing.Size(552, 32);
            this.splitContainer2.SplitterDistance = 87;
            this.splitContainer2.TabIndex = 0;
            // 
            // uxSelectPathCommand
            // 
            this.uxSelectPathCommand.AutoSize = true;
            this.uxSelectPathCommand.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.uxSelectPathCommand.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.uxSelectPathCommand.Depth = 0;
            this.uxSelectPathCommand.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uxSelectPathCommand.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.uxSelectPathCommand.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.uxSelectPathCommand.Location = new System.Drawing.Point(0, 0);
            this.uxSelectPathCommand.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.uxSelectPathCommand.MouseState = MaterialSkin.MouseState.HOVER;
            this.uxSelectPathCommand.Name = "uxSelectPathCommand";
            this.uxSelectPathCommand.Primary = false;
            this.uxSelectPathCommand.Size = new System.Drawing.Size(87, 32);
            this.uxSelectPathCommand.TabIndex = 25;
            this.uxSelectPathCommand.Text = "開く";
            this.uxSelectPathCommand.UseVisualStyleBackColor = false;
            this.uxSelectPathCommand.Click += new System.EventHandler(this.UxSelectMDBFolderCommand_Click);
            // 
            // uxSelectedPathText
            // 
            this.uxSelectedPathText.AccessibleName = "1234";
            this.uxSelectedPathText.Depth = 0;
            this.uxSelectedPathText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uxSelectedPathText.Hint = "empty";
            this.uxSelectedPathText.Location = new System.Drawing.Point(0, 5);
            this.uxSelectedPathText.MouseState = MaterialSkin.MouseState.HOVER;
            this.uxSelectedPathText.Name = "uxSelectedPathText";
            this.uxSelectedPathText.PasswordChar = '\0';
            this.uxSelectedPathText.SelectedText = "";
            this.uxSelectedPathText.SelectionLength = 0;
            this.uxSelectedPathText.SelectionStart = 0;
            this.uxSelectedPathText.Size = new System.Drawing.Size(461, 23);
            this.uxSelectedPathText.TabIndex = 26;
            this.uxSelectedPathText.UseSystemPasswordChar = false;
            // 
            // uxIndexTitleText
            // 
            this.uxIndexTitleText.Depth = 0;
            this.uxIndexTitleText.Hint = "";
            this.uxIndexTitleText.Location = new System.Drawing.Point(101, 653);
            this.uxIndexTitleText.MouseState = MaterialSkin.MouseState.HOVER;
            this.uxIndexTitleText.Name = "uxIndexTitleText";
            this.uxIndexTitleText.PasswordChar = '\0';
            this.uxIndexTitleText.SelectedText = "";
            this.uxIndexTitleText.SelectionLength = 0;
            this.uxIndexTitleText.SelectionStart = 0;
            this.uxIndexTitleText.Size = new System.Drawing.Size(207, 23);
            this.uxIndexTitleText.TabIndex = 3;
            this.uxIndexTitleText.UseSystemPasswordChar = false;
            // 
            // uxCloseCommand
            // 
            this.uxCloseCommand.AutoSize = true;
            this.uxCloseCommand.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.uxCloseCommand.Depth = 0;
            this.uxCloseCommand.Location = new System.Drawing.Point(477, 653);
            this.uxCloseCommand.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.uxCloseCommand.MouseState = MaterialSkin.MouseState.HOVER;
            this.uxCloseCommand.Name = "uxCloseCommand";
            this.uxCloseCommand.Primary = false;
            this.uxCloseCommand.Size = new System.Drawing.Size(49, 36);
            this.uxCloseCommand.TabIndex = 2;
            this.uxCloseCommand.Text = "閉じる";
            this.uxCloseCommand.UseVisualStyleBackColor = true;
            this.uxCloseCommand.Click += new System.EventHandler(this.uxCloseCommand_Click);
            // 
            // uxAddIndexCommand
            // 
            this.uxAddIndexCommand.AutoSize = true;
            this.uxAddIndexCommand.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.uxAddIndexCommand.Depth = 0;
            this.uxAddIndexCommand.Location = new System.Drawing.Point(349, 653);
            this.uxAddIndexCommand.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.uxAddIndexCommand.MouseState = MaterialSkin.MouseState.HOVER;
            this.uxAddIndexCommand.Name = "uxAddIndexCommand";
            this.uxAddIndexCommand.Primary = false;
            this.uxAddIndexCommand.Size = new System.Drawing.Size(120, 36);
            this.uxAddIndexCommand.TabIndex = 1;
            this.uxAddIndexCommand.Text = "インデックスの追加";
            this.uxAddIndexCommand.UseVisualStyleBackColor = true;
            this.uxAddIndexCommand.Click += new System.EventHandler(this.uxAddIndexCommand_Click);
            // 
            // uxSelectedPathItemsDLV
            // 
            this.uxSelectedPathItemsDLV.CellEditUseWholeCell = false;
            this.uxSelectedPathItemsDLV.DataSource = null;
            this.uxSelectedPathItemsDLV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uxSelectedPathItemsDLV.EmptyListMsg = "No MDB Files";
            this.uxSelectedPathItemsDLV.HideSelection = false;
            this.uxSelectedPathItemsDLV.Location = new System.Drawing.Point(0, 0);
            this.uxSelectedPathItemsDLV.Name = "uxSelectedPathItemsDLV";
            this.uxSelectedPathItemsDLV.ShowGroups = false;
            this.uxSelectedPathItemsDLV.ShowItemToolTips = true;
            this.uxSelectedPathItemsDLV.Size = new System.Drawing.Size(552, 695);
            this.uxSelectedPathItemsDLV.TabIndex = 0;
            this.uxSelectedPathItemsDLV.UseCompatibleStateImageBehavior = false;
            this.uxSelectedPathItemsDLV.View = System.Windows.Forms.View.Details;
            // 
            // IndexSelectorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(558, 799);
            this.Controls.Add(this.splitContainer1);
            this.Name = "IndexSelectorForm";
            this.Padding = new System.Windows.Forms.Padding(3, 65, 3, 3);
            this.Sizable = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "IndexSelectorForm";
            this.Load += new System.EventHandler(this.IndexSelectorForm_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uxSelectedPathItemsDLV)).EndInit();
            this.ResumeLayout(false);

        }



        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private MaterialSkin.Controls.MaterialFlatButton uxSelectPathCommand;
        private MaterialSkin.Controls.MaterialSingleLineTextField uxSelectedPathText;
        private BrightIdeasSoftware.DataListView uxSelectedPathItemsDLV;
        private MaterialSkin.Controls.MaterialFlatButton uxAddIndexCommand;
        private MaterialSkin.Controls.MaterialFlatButton uxCloseCommand;
        private MaterialSkin.Controls.MaterialSingleLineTextField uxIndexTitleText;
    }
}