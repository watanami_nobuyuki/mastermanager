﻿using System.Collections.Generic;

namespace INTEC.Med.MasterManager.ViewsAbstraction
{
    public interface IIndexSelectorView : IBaseView
    {
        string FormTitle { get; set; }

        string DefaultOpenPath { get; set; }

        void CloseView();

        event GetLastOpenPathHandler _getLastOpenPath;

        event SetLastOpenPathHandler _setLastOpenPath;

        event IsContainsMDBHandler _isContaninsMDB;

        event GetMDBFileListHandler _getMDBFileList;

        event AddIndexHandler _addIndex;
    }

    public delegate string GetLastOpenPathHandler();

    public delegate void SetLastOpenPathHandler(string arg);

    public delegate bool IsContainsMDBHandler(string arg);

    public delegate List<string> GetMDBFileListHandler(string arg);

    public delegate void AddIndexHandler(string arg, string arg2);
}