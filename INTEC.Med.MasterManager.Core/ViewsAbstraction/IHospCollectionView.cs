﻿namespace INTEC.Med.MasterManager.ViewsAbstraction
{
    public interface IHospCollectionView : IBaseView
    {
        string TabTitle { get; set; }

        event ShowIndexSelectorViewHandler _showIndexSelectorView;
    }

    public delegate void ShowIndexSelectorViewHandler(IIndexSelectorView m);
}