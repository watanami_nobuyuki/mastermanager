﻿namespace INTEC.Med.MasterManager.ViewsAbstraction
{
    public interface ITableCollectionView : IBaseView
    {
        string TabTitle { get; set; }
        string TableFilterText { get; set; }

        event StoreTableCollection _storeTableCollection;
    }

    public delegate void StoreTableCollection();
}