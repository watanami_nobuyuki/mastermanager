﻿using INTEC.Med.MasterManager.ViewsAbstraction;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace INTEC.Med.MasterManager.Presenters
{
    public class IndexSelectorPresenter
    {
        #region メンバ変数

        private readonly IIndexSelectorView _IndexSelectorView;

        #endregion メンバ変数

        public IndexSelectorPresenter(IIndexSelectorView IndexSelectorView)
        {
            _IndexSelectorView = IndexSelectorView;
            _IndexSelectorView._getLastOpenPath += GetLastOpenPath;
            _IndexSelectorView._setLastOpenPath += SetLastOpenPath;
            _IndexSelectorView._isContaninsMDB += IsContainsMDB;
            _IndexSelectorView._getMDBFileList += GetMDBFileList;
            _IndexSelectorView._addIndex += AddIndex;

            _IndexSelectorView.FormTitle = CommonConstant.Forms.IndexSelectorTitleConst;
            _IndexSelectorView.DefaultOpenPath = CommonConstant.Commons.defaultOpenPathConst;
        }

        #region Viewから受け取ったイベント

        private string GetLastOpenPath()
        {
            return SettingReader.LastOpenPath;
        }

        private void SetLastOpenPath(string arg)
        {
            SettingReader.LastOpenPath = arg;
        }

        /// <summary>
        /// 引数のPathにMDBを含んでいるか
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private bool IsContainsMDB(string arg)
        {
            bool isContainMDB = Directory.EnumerateFiles(arg, CommonConstant.Commons.filterExtensionConst).Any();
            if (!isContainMDB)
            {
                _IndexSelectorView.HandleError(new System.Exception(CommonConstant.Messages.noMDBFoundConst));
            }
            return isContainMDB;
        }

        /// <summary>
        /// 引数のPathにあるMDBをフルネームで返す
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private List<string> GetMDBFileList(string arg)
        {
            return Directory.EnumerateFiles(arg, CommonConstant.Commons.filterExtensionConst).Select(x => Path.GetFullPath(x)).ToList();
        }

        /// <summary>
        /// IndexをXMLに追加する
        /// </summary>
        /// <param name="arg">Indexに追加するパス</param>
        private void AddIndex(string arg, string arg2)
        {
            IndexInfo indexinfo = new IndexInfo();
            indexinfo.IndexTitle = arg2;
            indexinfo.IndexPath = arg;
            using (StreamWriter _write = new StreamWriter(Path.Combine(Application.UserAppDataPath, "Index.xml"), false, Encoding.UTF8))
            {
                XmlSerializer _serializer = new XmlSerializer(typeof(IndexInfo));
                _serializer.Serialize(_write, indexinfo);
            }

            _IndexSelectorView.CloseView();
        }

        #endregion Viewから受け取ったイベント
    }
}