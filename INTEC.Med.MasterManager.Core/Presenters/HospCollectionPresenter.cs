﻿using INTEC.Med.CommonLibrary;
using INTEC.Med.MasterManager.ViewsAbstraction;
using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;

namespace INTEC.Med.MasterManager.Presenters
{
    public class HospCollectionPresenter
    {
        #region _member変数

        private ILog _log = LogFetcher.GetLogger();
        private readonly IHospCollectionView _HospCollectionView;

        #endregion _member変数

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="HospCollectionView"></param>
        public HospCollectionPresenter(IHospCollectionView HospCollectionView)
        {
            _HospCollectionView = HospCollectionView;
            _HospCollectionView._showIndexSelectorView += ShowIndexSelectorView;

            //各値を定数クラスから持ってくる
            _HospCollectionView.TabTitle = CommonConstant.Forms.hospCollectionTabTilteConst;
        }

        #region Viewから受け取った処理

        /// <summary>
        /// 選択したフォルダ内のMDBを一覧化してXMLシリアライズ
        /// </summary>
        /// <param name="arg"></param>
        private void _FetchHospInfo(string selectedPath)
        {
            //フォルダ内にあるMDBをLISTで取得する
            List<string> IndexSelector = GetIndexSelector(selectedPath);
        }

        /// <summary>
        /// 子プレゼンターに子フォームのインスタンスを持たせる
        /// ※子フォームのインスタンスは親フォームで作る
        /// </summary>
        /// <param name="IndexSelectorView"></param>
        private void ShowIndexSelectorView(IIndexSelectorView IndexSelectorView)
        {
            var IndexSelectorPresenter = new IndexSelectorPresenter(IndexSelectorView);
            IndexSelectorView.ShowView();
        }

        #endregion Viewから受け取った処理

        /// <summary>
        /// 指定フォルダ内のMDBファイル一覧を取得する
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private List<string> GetIndexSelector(string path)
        {
            _log.Info(path + "配下のMDB一覧を取得します。");
            List<string> IndexSelector = Directory.GetFiles(path)
                        .Where(x => Path.GetExtension(x) == CommonConstant.Commons.filterExtensionConst)
                        .Select(x => Path.GetFileName(x)).ToList();

            //MDBファイルが見つからなければエラーとする
            if (IndexSelector.Count() == 0)
            {
                System.ArgumentException ex = new ArgumentException(CommonConstant.Messages.noMDBFoundConst, path);
                _HospCollectionView.HandleError(ex);
                _log.Info(path + ex.Message);
                return null;
            }

            return IndexSelector;
        }
    }
}