﻿using INTEC.Med.Com.DaoManager;
using INTEC.Med.MasterManager.ViewsAbstraction;
using System.IO;
using System.Linq;

namespace INTEC.Med.MasterManager.Presenters
{
    public class TableCollectionPresenter
    {
        #region メンバ変数

        private readonly ITableCollectionView _TableCollectionView;

        #endregion メンバ変数

        public TableCollectionPresenter(ITableCollectionView TableCollectionView)
        {
            _TableCollectionView = TableCollectionView;
            _TableCollectionView.TabTitle = CommonConstant.Forms.tableCollectionTabTitleConst;
            _TableCollectionView._storeTableCollection += StoreTableCollection;
        }

        /// <summary>
        ///  テーブル一覧の値を引っ張ってきて渡す
        /// </summary>
        private void StoreTableCollection()
        {
            string targetPath = CommonConstant.Commons.defaultOpenPathConst;
            string extension = CommonConstant.Commons.filterExtensionConst;

            //フォルダ存在するかチェック
            if (!Directory.Exists(targetPath))
            {
                _TableCollectionView.HandleError(new System.Exception(CommonConstant.Messages.noDirectoryFoundConst + targetPath));
                return;
            }

            //フォルダに存在するMDBをListに
            var mdbList = Directory.EnumerateFiles(targetPath, extension).Select(x => Path.GetFullPath(x)).ToList();

            if (mdbList.Count() == 0)
            {
                _TableCollectionView.HandleError(new System.Exception(CommonConstant.Messages.noMDBFoundConst));
                return;
            }

            //テーブル一覧の取得
            foreach (string mdbName in mdbList)
            {
                DaoManager daoManager = new DaoManager(mdbName);
                var tableList = daoManager.StoreTableList();
            }

            /*
             * tablelist[1].tableName
             * tablelist[1].mdbFullName
             * tablelist[1].tableDescription
             *
             */
        }
    }
}